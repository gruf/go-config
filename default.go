package config

import (
	"io"
	"time"

	"codeberg.org/gruf/go-bytesize"
	"github.com/pelletier/go-toml/v2"
)

// global is the global Config instance.
var global Config

func ParseFile(path string) error {
	return global.ParseFile(path)
}

func Encode(w io.Writer) error {
	return global.Encode(w)
}

func Decode(r io.Reader, strict bool) error {
	return global.Decode(r, strict)
}

func Reset() {
	global.Reset()
}

func Var(ptr interface{}, key toml.Key, value interface{}) {
	global.Var(ptr, key, value)
}

func StringVar(ptr *string, key toml.Key, value string) {
	global.StringVar(ptr, key, value)
}

func String(key toml.Key, value string) *string {
	return global.String(key, value)
}

func IntVar(ptr *int64, key toml.Key, value int64) {
	global.IntVar(ptr, key, value)
}

func Int(key toml.Key, value int64) *int64 {
	return global.Int(key, value)
}

func UintVar(ptr *uint64, key toml.Key, value uint64) {
	global.UintVar(ptr, key, value)
}

func Uint(key toml.Key, value uint64) *uint64 {
	return global.Uint(key, value)
}

func FloatVar(ptr *float64, key toml.Key, value float64) {
	global.FloatVar(ptr, key, value)
}

func Float(key toml.Key, value float64) *float64 {
	return global.Float(key, value)
}

func BoolVar(ptr *bool, key toml.Key, value bool) {
	global.BoolVar(ptr, key, value)
}

func Bool(key toml.Key, value bool) *bool {
	return global.Bool(key, value)
}

func TimeVar(ptr *time.Time, key toml.Key, value time.Time) {
	global.TimeVar(ptr, key, value)
}

func Time(key toml.Key, value time.Time) *time.Time {
	return global.Time(key, value)
}

func DurationVar(ptr *time.Duration, key toml.Key, value time.Duration) {
	global.DurationVar(ptr, key, value)
}

func Duration(key toml.Key, value time.Duration) *time.Duration {
	return global.Duration(key, value)
}

func SizeVar(ptr *bytesize.Size, key toml.Key, value bytesize.Size) {
	global.SizeVar(ptr, key, value)
}

func Size(key toml.Key, value bytesize.Size) *bytesize.Size {
	return global.Size(key, value)
}

func StringArrayVar(ptr *[]string, key toml.Key, value []string) {
	global.StringArrayVar(ptr, key, value)
}

func StringArray(key toml.Key, value []string) *[]string {
	return global.StringArray(key, value)
}

func IntArrayVar(ptr *[]int64, key toml.Key, value []int64) {
	global.IntArrayVar(ptr, key, value)
}

func IntArray(key toml.Key, value []int64) *[]int64 {
	return global.IntArray(key, value)
}

func UintArrayVar(ptr *[]uint64, key toml.Key, value []uint64) {
	global.UintArrayVar(ptr, key, value)
}

func UintArray(key toml.Key, value []uint64) *[]uint64 {
	return global.UintArray(key, value)
}

func FloatArrayVar(ptr *[]float64, key toml.Key, value []float64) {
	global.FloatArrayVar(ptr, key, value)
}

func FloatArray(key toml.Key, value []float64) *[]float64 {
	return global.FloatArray(key, value)
}

func BoolArrayVar(ptr *[]bool, key toml.Key, value []bool) {
	global.BoolArrayVar(ptr, key, value)
}

func BoolArray(key toml.Key, value []bool) *[]bool {
	return global.BoolArray(key, value)
}

func TimeArrayVar(ptr *[]time.Time, key toml.Key, value []time.Time) {
	global.TimeArrayVar(ptr, key, value)
}

func TimeArray(key toml.Key, value []time.Time) *[]time.Time {
	return global.TimeArray(key, value)
}

func DurationArrayVar(ptr *[]time.Duration, key toml.Key, value []time.Duration) {
	global.DurationArrayVar(ptr, key, value)
}

func DurationArray(key toml.Key, value []time.Duration) *[]time.Duration {
	return global.DurationArray(key, value)
}

func SizeArrayVar(ptr *[]bytesize.Size, key toml.Key, value []bytesize.Size) {
	global.SizeArrayVar(ptr, key, value)
}

func SizeArray(key toml.Key, value []bytesize.Size) *[]bytesize.Size {
	return global.SizeArray(key, value)
}
