package config

import (
	"fmt"
	"io"
	"os"

	toml "github.com/pelletier/go-toml/v2"
)

// ParseFile will parse the TOML file at path.
func (c *Config) ParseFile(path string) error {
	f, err := os.Open(path)
	if err != nil {
		return err
	}
	return c.Decode(f, true)
}

// Decode will decode TOML contents in io.Reader. Strict specifies whether to error on unknown keys.
func (c *Config) Decode(r io.Reader, strict bool) error {
	var raw map[string]interface{}

	// Wrap reader in TOML decoder
	dec := toml.NewDecoder(r)

	// Decode TOML into our raw map
	if err := dec.Decode(&raw); err != nil {
		return err
	}

	// Decode raw map into ptr values
	unknown, err := decodeRaw(c.root, "", raw)
	if err != nil {
		return err
	}

	// Check for unknown keys in raw
	if strict && len(unknown) > 0 {
		return fmt.Errorf("unknown keys: %v", unknown)
	}

	return nil
}

// decodeRaw unmarshals each of the values in a raw TOML map into their value pointers.
func decodeRaw(n *node, path string, level map[string]interface{}) ([]string, error) {
	var unknown []string

	for key, val := range level {
		var child *node

		// Build current keypath
		keypath := keypath(path, key)

		if n != nil {
			// Try get child for key
			child = n.GetChild(key)
		}

		// Check if this is a submap
		submap, cast := val.(map[string]interface{})

		if child == nil || child.Ptr() == nil {
			// child = nil indicates this is a
			// totally unknown TOML table
			//
			// child.Ptr() = nil indicates this
			// is a non-leaf node in a table
			//

			if cast {
				// This is a further submap
				keys, err := decodeRaw(child, keypath, submap)
				if err != nil {
					return nil, err
				}

				// Append any extra unknown keys
				unknown = append(unknown, keys...)
				continue
			}

			// This is an unknown key
			unknown = append(unknown, keypath)
			continue
		}

		if cast {
			// This is a map value
			err := child.Ptr().SetMap(submap)
			if err != nil {
				return nil, decodeError(keypath, submap, err)
			}
			continue
		}

		if v, ok := val.([]interface{}); ok {
			// This is a slice value
			err := child.Ptr().SetSlice(v)
			if err != nil {
				return nil, decodeError(keypath, v, err)
			}
			continue
		}

		// This is a regular value
		err := child.Ptr().SetValue(val)
		if err != nil {
			return nil, decodeError(keypath, val, err)
		}
	}

	return unknown, nil
}
