module codeberg.org/gruf/go-config

go 1.14

require (
	codeberg.org/gruf/go-bytes v1.0.2
	codeberg.org/gruf/go-bytesize v0.1.0
	github.com/mitchellh/mapstructure v1.4.3
	github.com/pelletier/go-toml/v2 v2.0.0-beta.5
)
