package config

import (
	"time"

	"codeberg.org/gruf/go-bytes"
	"codeberg.org/gruf/go-bytesize"
)

// Unmarshaler defines a type capable of being unmarshaled from default TOML types.
type Unmarshaler interface {
	UnmarshalTOML(interface{}) error
}

// duration provides marshal/unmarshalers for time.Duration.
type duration time.Duration

func (d duration) MarshalText() ([]byte, error) {
	str := (time.Duration)(d).String()
	return bytes.StringToBytes(str), nil
}

func (s *size) UnmarshalTOML(i interface{}) error {
	str, ok := i.(string)
	if !ok {
		return typeError(i, "string")
	}
	v, err := bytesize.ParseSize(str)
	if err != nil {
		return err
	}
	*s = size(v)
	return nil
}

// size provides marshal/unmarshalers for bytesize.Size.
type size bytesize.Size

func (s size) MarshalText() ([]byte, error) {
	b := (bytesize.Size)(s).AppendFormatIEC(nil)
	return b, nil
}

func (d *duration) UnmarshalTOML(i interface{}) error {
	str, ok := i.(string)
	if !ok {
		return typeError(i, "string")
	}
	v, err := time.ParseDuration(str)
	if err != nil {
		return err
	}
	*d = duration(v)
	return nil
}
