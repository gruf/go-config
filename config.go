package config

import (
	"fmt"
	"reflect"
	"time"
	"unsafe"

	"codeberg.org/gruf/go-bytesize"
	"github.com/pelletier/go-toml/v2"
)

// Config represents a variable (by value pointer) tracking system for TOML configuration files.
type Config struct {
	root *node
}

// register will register the value pointer under given key, with provided default value.
func (c *Config) register(ptr interface{}, key toml.Key, value interface{}) {
	// Ensure init'd
	if c.root == nil {
		c.root = &node{}
	}

	// Wrap ptr in valuePtr
	vptr := getValuePtr(ptr)

	// Attempt to assign default
	if v := reflect.ValueOf(value); !vptr.Assign(v) {
		panic(fmt.Sprintf("invalid default value (%s)(%v) for ptr *%s", v.Type(), value, vptr.typ))
	}

	// Append value ptr
	c.root.Add(key, vptr)
}

// Reset will reset this Config, dropping all value pointers.
func (c *Config) Reset() {
	c.root = nil
}

// Var will register the pointer 'ptr' under given key, with provided default value.
func (c *Config) Var(ptr interface{}, key toml.Key, value interface{}) {
	c.register(ptr, key, value)
}

// StringVar registers the string pointer 'ptr' under given key, with provided default value.
func (c *Config) StringVar(ptr *string, key toml.Key, value string) {
	c.register(ptr, key, value)
}

// String registers a new string pointer under given key, with provided default value. Returns this pointer.
func (c *Config) String(key toml.Key, value string) *string {
	s := new(string)
	c.StringVar(s, key, value)
	return s
}

// IntVar registers the int64 pointer 'ptr' under given key, with provided default value.
func (c *Config) IntVar(ptr *int64, key toml.Key, value int64) {
	c.register(ptr, key, value)
}

// Int registers a new int64 pointer under given key, with provided default value. Returns this pointer.
func (c *Config) Int(key toml.Key, value int64) *int64 {
	i := new(int64)
	c.IntVar(i, key, value)
	return i
}

// UintVar registers the uint64 pointer 'ptr' under given key, with provided default value.
func (c *Config) UintVar(ptr *uint64, key toml.Key, value uint64) {
	c.register(ptr, key, value)
}

// Uint registers a new uint64 pointer under given key, with provided default value. Returns this pointer.
func (c *Config) Uint(key toml.Key, value uint64) *uint64 {
	u := new(uint64)
	c.UintVar(u, key, value)
	return u
}

// FloatVar registers the float64 pointer 'ptr' under given key, with provided default value.
func (c *Config) FloatVar(ptr *float64, key toml.Key, value float64) {
	c.register(ptr, key, value)
}

// Float registers a new float64 pointer under given key, with provided default value. Returns this pointer.
func (c *Config) Float(key toml.Key, value float64) *float64 {
	f := new(float64)
	c.FloatVar(f, key, value)
	return f
}

// BoolVar registers the bool pointer 'ptr' under given key, with provided default value.
func (c *Config) BoolVar(ptr *bool, key toml.Key, value bool) {
	c.register(ptr, key, value)
}

// Bool registers a new bool pointer under given key, with provided default value. Returns this pointer.
func (c *Config) Bool(key toml.Key, value bool) *bool {
	b := new(bool)
	c.BoolVar(b, key, value)
	return b
}

// TimeVar registers the time.Time pointer 'ptr' under given key, with provided default value.
func (c *Config) TimeVar(ptr *time.Time, key toml.Key, value time.Time) {
	c.register(ptr, key, value)
}

// Time registers a new time.Time pointer under given key, with provided default value. Returns this pointer.
func (c *Config) Time(key toml.Key, value time.Time) *time.Time {
	t := new(time.Time)
	c.TimeVar(t, key, value)
	return t
}

// DurationVar registers the time.Duration pointer 'ptr' under given key, with provided default value.
func (c *Config) DurationVar(ptr *time.Duration, key toml.Key, value time.Duration) {
	c.register((*duration)(ptr), key, duration(value))
}

// Duration registers a new time.Duration pointer under given key, with provided default value. Returns this pointer.
func (c *Config) Duration(key toml.Key, value time.Duration) *time.Duration {
	d := new(time.Duration)
	c.DurationVar(d, key, value)
	return d
}

// SizeVar registers the bytesize.Size pointer 'ptr' under given key, with provided default value.
func (c *Config) SizeVar(ptr *bytesize.Size, key toml.Key, value bytesize.Size) {
	c.register((*size)(ptr), key, size(value))
}

// Size registers a new bytesize.Size pointer under given key, with provided default value. Returns this pointer.
func (c *Config) Size(key toml.Key, value bytesize.Size) *bytesize.Size {
	s := new(bytesize.Size)
	c.SizeVar(s, key, value)
	return s
}

// StringArrayVar registers the []string pointer 'ptr' under given key, with provided default value.
func (c *Config) StringArrayVar(ptr *[]string, key toml.Key, value []string) {
	c.register(ptr, key, value)
}

// StringArray registers a new []string pointer under given key, with provided default value. Returns this pointer.
func (c *Config) StringArray(key toml.Key, value []string) *[]string {
	s := new([]string)
	c.StringArrayVar(s, key, value)
	return s
}

// IntArrayVar registers the []int64 pointer 'ptr' under given key, with provided default value.
func (c *Config) IntArrayVar(ptr *[]int64, key toml.Key, value []int64) {
	c.register(ptr, key, value)
}

// IntArray registers a new []int64 pointer under given key, with provided default value. Returns this pointer.
func (c *Config) IntArray(key toml.Key, value []int64) *[]int64 {
	i := new([]int64)
	c.IntArrayVar(i, key, value)
	return i
}

// UintArrayVar registers the []uint64 pointer 'ptr' under given key, with provided default value.
func (c *Config) UintArrayVar(ptr *[]uint64, key toml.Key, value []uint64) {
	c.register(ptr, key, value)
}

// UintArray registers a new []uint64 pointer under given key, with provided default value. Returns this pointer.
func (c *Config) UintArray(key toml.Key, value []uint64) *[]uint64 {
	u := new([]uint64)
	c.UintArrayVar(u, key, value)
	return u
}

// FloatArrayVar registers the []float64 pointer 'ptr' under given key, with provided default value.
func (c *Config) FloatArrayVar(ptr *[]float64, key toml.Key, value []float64) {
	c.register(ptr, key, value)
}

// FloatArray registers a new []float64 pointer under given key, with provided default value. Returns this pointer.
func (c *Config) FloatArray(key toml.Key, value []float64) *[]float64 {
	f := new([]float64)
	c.FloatArrayVar(f, key, value)
	return f
}

// BoolArrayVar registers the []bool pointer 'ptr' under given key, with provided default value.
func (c *Config) BoolArrayVar(ptr *[]bool, key toml.Key, value []bool) {
	c.register(ptr, key, value)
}

// BoolArray registers a new []bool pointer under given key, with provided default value. Returns this pointer.
func (c *Config) BoolArray(key toml.Key, value []bool) *[]bool {
	b := new([]bool)
	c.BoolArrayVar(b, key, value)
	return b
}

// TimeArrayVar registers the []time.Time pointer 'ptr' under given key, with provided default value.
func (c *Config) TimeArrayVar(ptr *[]time.Time, key toml.Key, value []time.Time) {
	c.register(ptr, key, value)
}

// TimeArray registers a new []time.Time pointer under given key, with provided default value. Returns this pointer.
func (c *Config) TimeArray(key toml.Key, value []time.Time) *[]time.Time {
	t := new([]time.Time)
	c.TimeArrayVar(t, key, value)
	return t
}

// DurationArrayVar registers the []time.Duration pointer 'ptr' under given key, with provided default value.
func (c *Config) DurationArrayVar(ptr *[]time.Duration, key toml.Key, value []time.Duration) {
	c.register((*[]duration)(unsafe.Pointer(ptr)), key, *(*[]duration)(unsafe.Pointer(&value)))
}

// DurationArray registers a new []time.Duration pointer under given key, with provided default value. Returns this pointer.
func (c *Config) DurationArray(key toml.Key, value []time.Duration) *[]time.Duration {
	d := new([]time.Duration)
	c.DurationArrayVar(d, key, value)
	return d
}

// SizeArrayVar registers the []bytesize.Size pointer 'ptr' under given key, with provided default value.
func (c *Config) SizeArrayVar(ptr *[]bytesize.Size, key toml.Key, value []bytesize.Size) {
	c.register((*[]size)(unsafe.Pointer(ptr)), key, *(*[]size)(unsafe.Pointer(&value)))
}

// SizeArray registers a new []bytesize.Size pointer under given key, with provided default value. Returns this pointer.
func (c *Config) SizeArray(key toml.Key, value []bytesize.Size) *[]bytesize.Size {
	s := new([]bytesize.Size)
	c.SizeArrayVar(s, key, value)
	return s
}
