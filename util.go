package config

import (
	"fmt"
)

// keypath builds a key path for given leading path and current level key.
func keypath(path string, key string) string {
	if path == "" {
		return key
	}
	return path + "." + key
}

// typeError returns an compatible TOML type error.
func typeError(rcv interface{}, want string) error {
	return fmt.Errorf("incompatible TOML type %T (want %s)", rcv, want)
}

// decodeError returns a descriptive decode error for given keypath, value and error msg.
func decodeError(keypath string, val interface{}, msg interface{}) error {
	return fmt.Errorf("key=%q value=%v error=%q", keypath, val, msg)
}
