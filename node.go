package config

import (
	"fmt"
	"strings"

	"github.com/pelletier/go-toml/v2"
)

// node represents a node in a prefix tree.
type node struct {
	key string    // current level key
	ptr *valuePtr // value ptr
	pr  *node     // parent node
	ch  []*node   // list of children
}

// Key returns the combined string representation of node keys leading up to this.
func (n *node) Key() string {
	if n.pr != nil && n.pr.key != "" {
		return n.pr.Key() + "." + n.key
	}
	return n.key
}

// Add will add a new node to the try with given key and value ptr.
func (n *node) Add(key toml.Key, ptr *valuePtr) {
	for len(key) > 0 {
		var child *node

		// Check for valid current key
		if key[0] == "" || strings.Contains(key[0], ".") {
			panic("invalid key: " + key[0])
		}

		// Look for child with key
		child = n.GetChild(key[0])

		if child != nil &&
			child.ptr != nil {
			// This is already a child
			// leaf node, conflicting key!
			key := strings.Join(key, ".")
			panic(fmt.Sprintf("key conflict: %q and %q", key, child.Key()))
		}

		if child == nil {
			// Create new child for key
			child = &node{
				key: key[0],
				pr:  n,
			}

			// Append to current node
			n.ch = append(n.ch, child)

			// Return if leaf key
			if len(key) == 1 {
				child.ptr = ptr
				return
			}
		}

		// Jump to next
		key = key[1:]
		n = child
	}
}

// GetChild looks for a direct child of node with key.
func (n *node) GetChild(key string) *node {
	for _, child := range n.ch {
		if child.key == key {
			return child
		}
	}
	return nil
}

// Ptr returns the currently set valuePtr (is nil if not leaf).
func (n *node) Ptr() *valuePtr {
	return n.ptr
}

// Interface returns a TOML marshalable interface representation of node.
func (n *node) Interface() interface{} {
	// Leaf just returns ptr
	if n.ptr != nil {
		return n.ptr.Value()
	}

	// This is NOT a leaf, we
	// need to build a child map
	chs := make(map[string]interface{})

	// Fetch their values
	for _, ch := range n.ch {
		chs[ch.key] = ch.Interface()
	}

	return chs
}
