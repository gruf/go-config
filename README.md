# go-config

A TOML configuration-file flag parser. Mostly mimics the official Go [flag library](https://golang.org/pkg/flag/) in usage.
