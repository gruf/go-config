package config

import (
	"reflect"

	"github.com/mitchellh/mapstructure"
)

var (
	// unmarshalerType is the reflect type of toml.Unmarshaler.
	unmarshalerType = reflect.TypeOf((*Unmarshaler)(nil)).Elem()

	// ifacemapType is the reflect type of map[string]interface{}.
	ifacemapType = reflect.TypeOf(map[string]interface{}{})
)

// valuePtr represents a settable value pointer registered by Config.
type valuePtr struct {
	ptr interface{}
	val reflect.Value
	typ reflect.Type
}

// getValuePtr wraps a ptr in our own valuePtr struct.
func getValuePtr(ptr interface{}) *valuePtr {
	// Check for valid value ptr
	v := reflect.ValueOf(ptr)
	if v.Kind() != reflect.Ptr {
		panic("ptr must be a pointer")
	}

	// Get underlying value
	v = v.Elem()
	t := v.Type()

	return &valuePtr{
		ptr: ptr,
		val: v,
		typ: t,
	}
}

// SetValue attempts to set (and decode if necessary) the underlying valuePtr from provided interface.
func (ptr *valuePtr) SetValue(i interface{}) error {
	// First attempt a TOML unmarshal
	if ok, err := ptr.unmarshal(i); ok {
		return err
	}

	// Next we try reflection
	v := reflect.ValueOf(i)

	// Attempt a direct conversion
	if ptr.Assign(v) {
		return nil
	}

	// return a generic type error
	return typeError(i, ptr.typ.String())
}

// SetSlice attempts to set (and decode if necessary) the underlying valuePtr from provided slice.
func (ptr *valuePtr) SetSlice(i []interface{}) error {
	// First attempt a TOML unmarshal
	if ok, err := ptr.unmarshal(i); ok {
		return err
	}

	// Next we try reflection
	v := reflect.ValueOf(i)

	// Attempt a direct conversion
	if ptr.Assign(v) {
		return nil
	}

	// Check if we can append slice
	if ok, err := ptr.appendSlice(v); ok {
		return err
	}

	// return a generic type error
	return typeError(i, ptr.typ.String())
}

// SetMap attempts to set (and decode if necessary) the underlying valuePtr from provided map.
func (ptr *valuePtr) SetMap(i map[string]interface{}) error {
	// First attempt a TOML unmarshal
	if ok, err := ptr.unmarshal(i); ok {
		return err
	}

	// Check for a direct conversion
	if ifacemapType.AssignableTo(ptr.typ) {
		v := reflect.ValueOf(i)
		ptr.val.Set(v)
		return nil
	}

	// Create a new map structure decoder
	dec, err := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		ErrorUnused:      true,
		ZeroFields:       true,
		Squash:           true,
		WeaklyTypedInput: false,
		TagName:          "toml",
		Result:           ptr.ptr,
	})
	if err != nil {
		panic(err)
	}

	// Attempt to decode provided map
	if err := dec.Decode(i); err != nil {
		return err
	}

	return nil
}

// unmarshal attempts a TOML unmarshal of 'i' into underlying valuePtr.
func (ptr *valuePtr) unmarshal(i interface{}) (bool, error) {
	um, ok := ptr.ptr.(Unmarshaler)
	if !ok {
		return false, nil
	}
	err := um.UnmarshalTOML(i)
	return true, err
}

// Assign attempts a direct assignment of 'v' to underlying valuePtr.
func (ptr *valuePtr) Assign(v reflect.Value) bool {
	var ok bool
	if ok = v.Type().AssignableTo(ptr.typ); ok {
		ptr.val.Set(v)
	}
	return ok
}

// Value returns the underlying value at ptr.
func (ptr *valuePtr) Value() interface{} {
	return ptr.val.Interface()
}

// appendSlice attempts to convert slice 'v' and append (well, replace) existing valuePtr slice.
func (ptr *valuePtr) appendSlice(v reflect.Value) (bool, error) {
	// Check if 'we' are a slice
	if ptr.typ.Kind() != reflect.Slice {
		return false, nil
	}

	// Get our slice elem type
	elem := ptr.typ.Elem()

	if elem.Implements(unmarshalerType) ||
		reflect.PtrTo(elem).Implements(unmarshalerType) {
		// Our value ptr slice contains elements
		// of type implementing toml.Unmarshaler
		next, err := unmarshalSlice(v, ptr.typ, elem)
		if err != nil {
			return true, err
		}
		ptr.val.Set(next)
	} else {
		// Our value ptr slice requires conversion
		next, err := convertSlice(v, ptr.typ, elem)
		if err != nil {
			return true, err
		}
		ptr.val.Set(next)
	}

	return true, nil
}

// unmarshalSlice TOML unmarshals elements of 'slice' into a new slice of type 'want', with element types of 'wantElem'.
func unmarshalSlice(slice reflect.Value, want, wantElem reflect.Type) (reflect.Value, error) {
	// Get input length
	length := slice.Len()

	// Create new slice of input capacity
	next := reflect.MakeSlice(want, 0, length)

	for i := 0; i < length; i++ {
		// Get next element
		elem := slice.Index(i)

		// Alloc new wanted-type elem ptr
		ptr := reflect.New(wantElem)

		// Call UnmarshalTOML on new allocated value
		ret := ptr.MethodByName("UnmarshalTOML").Call(
			[]reflect.Value{elem},
		)

		// Check the unmarshaler error return
		if err, _ := ret[0].Interface().(error); err != nil {
			return reflect.Value{}, err
		}

		// Append next element to slice
		next = reflect.Append(next, ptr.Elem())
	}

	return next, nil
}

// convertSlice converts elements of 'slice' into a new slice of type 'want', with element types of 'wantElem'.
func convertSlice(slice reflect.Value, want, wantElem reflect.Type) (reflect.Value, error) {
	// Get input length
	length := slice.Len()

	// Create new slice of input capacity
	next := reflect.MakeSlice(want, 0, length)

	for i := 0; i < length; i++ {
		// Get next element
		elem := slice.Index(i)

		// If this is an interface, follow
		if elem.Kind() == reflect.Interface {
			elem = elem.Elem()
		}

		// Ensure compatible kinds
		if elem.Kind() != wantElem.Kind() {
			return reflect.Value{}, typeError(elem.Interface(), wantElem.String())
		}

		// Convert to expected
		elem = elem.Convert(wantElem)

		// Append next element to slice
		next = reflect.Append(next, elem)
	}

	return next, nil
}
