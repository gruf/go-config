package config

import (
	"io"

	toml "github.com/pelletier/go-toml/v2"
)

// Encode will encode the current TOML configuration to writer.
func (c *Config) Encode(w io.Writer) error {
	if c.root == nil {
		panic("config not initialized")
	}
	enc := toml.NewEncoder(w)
	enc.SetIndentSymbol("    ")
	enc.SetIndentTables(true)
	return enc.Encode(c.root.Interface())
}
